# Author: Olivier Lenoir <olivier.len02@gmail.com>
# Created: 2023-01-15 15:15:44
# License: MIT, Copyright (c) 2023 Olivier Lenoir
# Project: pdflatex

all: pdf

.PHONY: pdf

.SILENT: pdf clean


pdf: clean
	for file in */*.tex */*/*.tex; do \
		echo $${file}; \
		pdflatex $${file}; \
		pdflatex $${file}; \
		pdflatex $${file}; \
		make clean; \
		done


clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.out
	rm -f *.nav
	rm -f *.toc
	rm -f *.lof
	rm -f *.lot
